#ifndef IMOVEL_H_
#define IMOVEL_H_
#include <iostream>
#include <string>

class Imovel{

    protected:
        std::string titulo;
        std::string rua;
        std::string numero;
        std::string bairro;
        std::string CEP;
        std::string cidade;
        double valor;
        bool disponibilidade;
    public:
        std::string getTitulo();
        std::string getRua();
        std::string getNumero();
        std::string getBairro();
        std::string getCEP();
        std::string getCidade();
        double getValor();
        bool getDisponibilidade();
        void setTitulo(std::string titulo);
        void setRua(std::string rua);
        void setNumero(std::string numero);
        void setBairro(std::string bairro);
        void setCEP(std::string CEP);
        void setCidade(std::string cidade);
        void setValor(double valor);
        void setDisponibilidade(bool disponibilidade);
        std::string getDisponibilidadeString();

};
#endif
