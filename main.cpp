#include <iostream>
#include <string>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include "Sistema.h"

using namespace std;

int main(){

    int flag = 1;
    char opcao;
    string sopcao;
    Sistema menu;
    menu.recuperarListaImoveis();

    while(flag){
        system("clear");
        cout << "1.Cadastrar\n2.Listar\n3.Buscar\n4.Remover\n5.Editar\n6.Salvar\n0.Sair" << endl;

        getline(cin, sopcao);
        opcao = sopcao[0];

        switch(opcao){
            case 49:
                system("clear");
                menu.cadastraImovel();
                break;
            case 50:
                system("clear");
                menu.listar();
                break;
            case 51:
                system("clear");
                menu.buscar(0);
                break;
            case 52:
                system("clear");
                menu.buscar(1);
                break;
            case 53:
                system("clear");
                menu.buscar(2);
                break;
            case 54:
                system("clear");
                menu.salvarListaImoveis();
                break;
            case 48:
                flag = 0;
                break;
            default:
                system("clear");
                cout << "opcao invalida" << endl;
                flag = 1;
                break;
        }
    }

    return 0;
}
