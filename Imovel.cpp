#include "Imovel.h"

//Imovel::Imovel(std::string t, std::string r, std::string n, std::string b, std::string cp, std::string c, double v, bool d){
//    titulo = t;
//    rua = r;
//    numero = n;
//    bairro = b;
//    CEP = cp;
//    cidade = c;
//    valor = v;
//    disponibilidade = d;
//}

std::string Imovel::getTitulo(){
    return titulo;
}

std::string Imovel::getRua(){
    return rua;
}

std::string Imovel::getNumero(){
    return numero;
}

std::string Imovel::getBairro(){
    return bairro;
}

std::string Imovel::getCEP(){
    return CEP;
}

std::string Imovel::getCidade(){
    return cidade;
}

double Imovel::getValor(){
    return valor;
}

bool Imovel::getDisponibilidade(){
    return disponibilidade;
}

std::string Imovel::getDisponibilidadeString(){
    if(disponibilidade == 0){
        return "Alugar";
    }
    if(disponibilidade == 1){
        return "Vender";
    }
}

void Imovel::setTitulo(std::string t){
    titulo = t;
}

void Imovel::setRua(std::string r){
    rua = r;
}

void Imovel::setNumero(std::string n){
    numero = n;
}

void Imovel::setBairro(std::string b){
    bairro = b;
}

void Imovel::setCEP(std::string cp){
    CEP = cp;
}

void Imovel::setCidade(std::string c){
    cidade = c;
}

void Imovel::setValor(double v){
    valor = v;
}

void Imovel::setDisponibilidade(bool d){
    disponibilidade = d;
}
