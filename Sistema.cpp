#include "Sistema.h"

void Sistema::criaCasa(){
    std::cout << "Titulo:" << std::endl;
    std::string t;
    std::getline(std::cin,t);
    std::cout << "Pavimentos:" << std::endl;
    int p;
    std::cin >> p;
    std::cin.ignore();
    std::cout << "Quartos:" << std::endl;
    int q;
    std::cin >> q;
    std::cin.ignore();
    std::cout << "Area do terreno:" << std::endl;
    double art;
    std::cin >> art;
    std::cin.ignore();
    std::cout << "Area construida:" << std::endl;
    double arc;
    std::cin >> arc;
    std::cin.ignore();
    std::cout << "Rua:" << std::endl;
    std::string r;
    std::getline(std::cin,r);
    std::cout << "Numero:" << std::endl;
    std::string n;
    std::getline(std::cin,n);
    std::cout << "Bairro:" << std::endl;
    std::string b;
    std::getline(std::cin,b);
    std::cout << "CEP:" << std::endl;
    std::string cp;
    std::getline(std::cin,cp);
    std::cout << "Cidade:" << std::endl;
    std::string c;
    std::getline(std::cin,c);
    std::cout << "Valor:" << std::endl;
    double v;
    std::cin >> v;
    std::cin.ignore();
    std::cout << "Disponilibilidade(Alugar = 0/Vender = 1):" << std::endl;
    bool d;
    std::cin >> d;
    std::cin.ignore();
    casa.push_back(Casa(p,q,art,arc,t,r,n,b,cp,c,v,d));
}

void Sistema::criaApartamento(){
    std::cout << "Titulo:" << std::endl;
    std::string t;
    std::getline(std::cin,t);
    std::cout << "Andar:" << std::endl;
    int a;
    std::cin >> a;
    std::cin.ignore();
    std::cout << "Quartos:" << std::endl;
    int q;
    std::cin >> q;
    std::cin.ignore();
    std::cout << "Area do terreno:" << std::endl;
    double art;
    std::cin >> art;
    std::cin.ignore();
    std::cout << "Posicao:" << std::endl;
    std::string p;
    std::getline(std::cin,p);
    std::cout << "valor do Condominio:" << std::endl;
    double con;
    std::cin >> con;
    std::cin.ignore();
    std::cout << "Vagas:" << std::endl;
    int va;
    std::cin >> va;
    std::cin.ignore();
    std::cout << "Rua:" << std::endl;
    std::string r;
    std::getline(std::cin,r);
    std::cout << "Numero:" << std::endl;
    std::string n;
    std::getline(std::cin,n);
    std::cout << "Bairro:" << std::endl;
    std::string b;
    std::getline(std::cin,b);
    std::cout << "CEP:" << std::endl;
    std::string cp;
    std::getline(std::cin,cp);
    std::cout << "Cidade:" << std::endl;
    std::string c;
    std::getline(std::cin,c);
    std::cout << "Valor:" << std::endl;
    double v;
    std::cin >> v;
    std::cin.ignore();
    std::cout << "Disponilibilidade(Alugar = 0/Vender = 1):" << std::endl;
    bool d;
    std::cin >> d;
    std::cin.ignore();
    apt.push_back(Apartamento(a,q,art,p,con,va,t,r,n,b,cp,c,v,d));
}

void Sistema::criaTerreno(){
    std::cout << "Titulo:" << std::endl;
    std::string t;
    std::getline(std::cin,t);
    std::cout << "Area do terreno:" << std::endl;
    double art;
    std::cin >> art;
    std::cin.ignore();
    std::cout << "Rua:" << std::endl;
    std::string r;
    std::getline(std::cin,r);
    std::cout << "Numero:" << std::endl;
    std::string n;
    std::getline(std::cin,n);
    std::cout << "Bairro:" << std::endl;
    std::string b;
    std::getline(std::cin,b);
    std::cout << "CEP:" << std::endl;
    std::string cp;
    std::getline(std::cin,cp);
    std::cout << "Cidade:" << std::endl;
    std::string c;
    std::getline(std::cin,c);
    std::cout << "Valor:" << std::endl;
    double v;
    std::cin >> v;
    std::cin.ignore();
    std::cout << "Disponilibilidade(Alugar = 0/Vender = 1):" << std::endl;
    bool d;
    std::cin >> d;
    std::cin.ignore();
    ter.push_back(Terreno(art,t,r,n,b,cp,c,v,d));
}


void Sistema::cadastraImovel(){
    int flag = 1;
    char opcao;
    std::string sopcao;

    while(flag){
        system("clear");
        std::cout << "1.Casa\n2.Apartamento\n3.Terreno\n0.Voltar" << std::endl;
        getline(std::cin,sopcao);
        opcao = sopcao[0];

        switch(opcao){
            case 49:
                system("clear");
                criaCasa();
                break;
            case 50:
                system("clear");
                criaApartamento();
                break;
            case 51:
                system("clear");
                criaTerreno();
                break;
            case 48:
                system("clear");
                flag = 0;
                break;
            default:
                system("clear");
                std::cout << "opcao invalida" << std::endl;
                flag = 1;
                break;
        }
    }
}

void Sistema::listar(){
    int flag = 1;
    char opcao;
    std::string sopcao;

    while(flag){
        std::cout << "1.Tudo\n2.Casa\n3.Apartamento\n4.Terreno\n5.Alugar\n6.Vender\n0.Voltar" << std::endl;
        getline(std::cin,sopcao);
        opcao = sopcao[0];

        switch(opcao){
            case 49:
                system("clear");
                listarCasa(0);
                listarApartamento(0);
                listarTerreno(0);
                break;
            case 50:
                system("clear");
                listarCasa(1);
                break;
            case 51:
                system("clear");
                listarApartamento(1);
                break;
            case 52:
                system("clear");
                listarTerreno(1);
                break;
            case 53:
                system("clear");
                alugaVende(0);
                break;
            case 54:
                system("clear");
                alugaVende(1);
                break;
            case 48:
                system("clear");
                flag = 0;
                break;
            default:
                system("clear");
                std::cout << "opcao invalida" << std::endl;
                flag = 1;
                break;
        }
    }
}

void Sistema::alugaVende(int o){
    if(o == 0){
        for(int i = 0;i < casa.size();i++){
            if(casa[i].getDisponibilidade() == 0){
                printaCasa(i);
            }
        }
        for(int i = 0;i < apt.size();i++){
            if(apt[i].getDisponibilidade() == 0){
                printaApartamento(i);
            }
        }
        for(int i = 0;i < ter.size();i++){
            if(ter[i].getDisponibilidade() == 0){
                printaTerreno(i);
            }
        }
    }
    if(o == 1){
        for(int i = 0;i < casa.size();i++){
            if(casa[i].getDisponibilidade() == 1){
                printaCasa(i);
            }
        }
        for(int i = 0;i < apt.size();i++){
            if(apt[i].getDisponibilidade() == 1){
                printaApartamento(i);
            }
        }
        for(int i = 0;i < ter.size();i++){
            if(ter[i].getDisponibilidade() == 1){
                printaTerreno(i);
            }
        }
    }
}

void Sistema::printaCasa(int i){
    std::cout << "Titulo: " << casa[i].getTitulo() << std::endl;
    std::cout << "Pavimentos: " << casa[i].getPavimentos() << std::endl;
    std::cout << "Quartos: " << casa[i].getQuartos() << std::endl;
    std::cout << "Area do terreno: " << casa[i].getAreaTerreno() << std::endl;
    std::cout << "Area construida: " << casa[i].getAreaConstruida() << std::endl;
    std::cout << "Rua: " << casa[i].getRua() << std::endl;
    std::cout << "Numero: " << casa[i].getNumero() << std::endl;
    std::cout << "Bairro: " << casa[i].getBairro() << std::endl;
    std::cout << "CEP: " << casa[i].getCEP() << std::endl;
    std::cout << "Cidade: " << casa[i].getCidade() << std::endl;
    printf("Valor: %.2lf\n", casa[i].getValor());
    std::cout << "Disponibilidade: " << casa[i].getDisponibilidadeString() << std::endl;
    std::cout << "" << std::endl;
}

void Sistema::printaApartamento(int i){
    std::cout << "Titulo: " << apt[i].getTitulo() << std::endl;
    std::cout << "Andar: " << apt[i].getAndar() << std::endl;
    std::cout << "Quartos: " << apt[i].getQuartos() << std::endl;
    std::cout << "Area: " << apt[i].getAreaTerreno() << std::endl;
    std::cout << "Posicao: " << apt[i].getPosicao() << std::endl;
    std::cout << "Condominio: " << apt[i].getCondominio() << std::endl;
    std::cout << "Vagas de garagem: " << apt[i].getVagas() << std::endl;
    std::cout << "Rua: " << apt[i].getRua() << std::endl;
    std::cout << "Numero: " << apt[i].getNumero() << std::endl;
    std::cout << "Bairro: " << apt[i].getBairro() << std::endl;
    std::cout << "CEP: " << apt[i].getCEP() << std::endl;
    std::cout << "Cidade: " << apt[i].getCidade() << std::endl;
    printf("Valor: %.2lf\n", apt[i].getValor());
    std::cout << "Disponibilidade: " << apt[i].getDisponibilidadeString() << std::endl;
    std::cout << "" << std::endl;
}

void Sistema::printaTerreno(int i){
    std::cout << "Titulo: " << ter[i].getTitulo() << std::endl;
    std::cout << "Area do terreno: " << ter[i].getAreaTerreno() << std::endl;
    std::cout << "Rua: " << ter[i].getRua() << std::endl;
    std::cout << "Numero: " << ter[i].getNumero() << std::endl;
    std::cout << "Bairro: " << ter[i].getBairro() << std::endl;
    std::cout << "CEP: " << ter[i].getCEP() << std::endl;
    std::cout << "Cidade: " << ter[i].getCidade() << std::endl;
    printf("Valor: %.2lf\n", ter[i].getValor());
    std::cout << "Disponibilidade: " << ter[i].getDisponibilidadeString() << std::endl;
    std::cout << "" << std::endl;
}

void Sistema::listarCasa(int o){
    if(o == 1){
        for(int i = 0;i < casa.size();i++){
            printaCasa(i);
        }
    }
    if(o==0){
        for(int i = 0;i < casa.size();i++){
            std::cout << "Casa: " << casa[i].getTitulo() << std::endl;
            std::cout << "" << std::endl;
        }
    }
}

void Sistema::listarApartamento(int o){
    if(o == 1){
        for(int i = 0;i < apt.size();i++){
            printaApartamento(i);
        }
    }
    if(o==0){
        for(int i = 0;i < apt.size();i++){
            std::cout << "Apartamento: " << apt[i].getTitulo() << std::endl;
            std::cout << "" << std::endl;
        }
    }
}

void Sistema::listarTerreno(int o){
    if(o == 1){
        for(int i = 0;i < ter.size();i++){
            printaTerreno(i);
        }
    }
    if(o==0){
        for(int i = 0;i < ter.size();i++){
            std::cout << "Terreno: " << ter[i].getTitulo() << std::endl;
            std::cout << "" << std::endl;
        }
    }
}

void Sistema::salvarListaImoveis(){
    std::ofstream myfile;
    myfile.open("lista.txt");
    for(int i = 0;i < casa.size();i++){
        myfile << casa[i].getTipo() << std::endl;
        myfile << casa[i].getPavimentos() << std::endl;
        myfile << casa[i].getQuartos() << std::endl;
        myfile << casa[i].getAreaTerreno() << std::endl;
        myfile << casa[i].getAreaConstruida() << std::endl;
        myfile << casa[i].getTitulo() << std::endl;
        myfile << casa[i].getRua() << std::endl;
        myfile << casa[i].getNumero() << std::endl;
        myfile << casa[i].getBairro() << std::endl;
        myfile << casa[i].getCEP() << std::endl;
        myfile << casa[i].getCidade() << std::endl;
        myfile << casa[i].getValor() << std::endl;
        myfile << casa[i].getDisponibilidade() << std::endl;
    }
    for(int i = 0;i < apt.size();i++){
        myfile << apt[i].getTipo() << std::endl;
        myfile << apt[i].getAndar() << std::endl;
        myfile << apt[i].getQuartos() << std::endl;
        myfile << apt[i].getAreaTerreno() << std::endl;
        myfile << apt[i].getPosicao() << std::endl;
        myfile << apt[i].getCondominio() << std::endl;
        myfile << apt[i].getVagas() << std::endl;
        myfile << apt[i].getTitulo() << std::endl;
        myfile << apt[i].getRua() << std::endl;
        myfile << apt[i].getNumero() << std::endl;
        myfile << apt[i].getBairro() << std::endl;
        myfile << apt[i].getCEP() << std::endl;
        myfile << apt[i].getCidade() << std::endl;
        myfile << apt[i].getValor() << std::endl;
        myfile << apt[i].getDisponibilidade() << std::endl;
    }
    for(int i = 0;i < ter.size();i++){
        myfile << ter[i].getTipo() << std::endl;
        myfile << ter[i].getAreaTerreno() << std::endl;
        myfile << ter[i].getTitulo() << std::endl;
        myfile << ter[i].getRua() << std::endl;
        myfile << ter[i].getNumero() << std::endl;
        myfile << ter[i].getBairro() << std::endl;
        myfile << ter[i].getCEP() << std::endl;
        myfile << ter[i].getCidade() << std::endl;
        myfile << ter[i].getValor() << std::endl;
        myfile << ter[i].getDisponibilidade() << std::endl;
    }
    myfile.close();
}

void Sistema::recuperarListaImoveis(){
    std::ifstream myfile;
    std::string stri;
    int tipo;
    myfile.open("lista.txt");

    while(myfile >> tipo){
        if(tipo == 0){
            int p;
            myfile >> p;
            myfile.ignore();
            int q;
            myfile >> q;
            myfile.ignore();
            double art;
            myfile >> art;
            myfile.ignore();
            double arc;
            myfile >> arc;
            myfile.ignore();
            std::string t;
            std::getline(myfile,t);
            std::string r;
            std::getline(myfile,r);
            std::string n;
            std::getline(myfile,n);
            std::string b;
            std::getline(myfile,b);
            std::string cp;
            std::getline(myfile,cp);
            std::string c;
            std::getline(myfile,c);
            double v;
            myfile >> v;
            myfile.ignore();
            bool d;
            myfile >> d;
            myfile.ignore();
            casa.push_back(Casa(p,q,art,arc,t,r,n,b,cp,c,v,d));
        }
        if(tipo == 1){    
            int a;
            myfile >> a;
            myfile.ignore();
            int q;
            myfile >> q;
            myfile.ignore();
            double art;
            myfile >> art;
            myfile.ignore();
            std::string p;
            std::getline(myfile,p);
            double con;
            myfile >> con;
            myfile.ignore();
            int va;
            myfile >> va;
            myfile.ignore();
            std::string t;
            std::getline(myfile,t);
            std::string r;
            std::getline(myfile,r);
            std::string n;
            std::getline(myfile,n);
            std::string b;
            std::getline(myfile,b);
            std::string cp;
            std::getline(myfile,cp);
            std::string c;
            std::getline(myfile,c);
            double v;
            myfile >> v;
            myfile.ignore();
            bool d;
            myfile >> d;
            myfile.ignore();
            apt.push_back(Apartamento(a,q,art,p,con,va,t,r,n,b,cp,c,v,d));
        }
        if(tipo == 2){
            double art;
            myfile >> art;
            myfile.ignore();
            std::string t;
            std::getline(myfile,t);
            std::string r;
            std::getline(myfile,r);
            std::string n;
            std::getline(myfile,n);
            std::string b;
            std::getline(myfile,b);
            std::string cp;
            std::getline(myfile,cp);
            std::string c;
            std::getline(myfile,c);
            double v;
            myfile >> v;
            myfile.ignore();
            bool d;
            myfile >> d;
            myfile.ignore();
            ter.push_back(Terreno(art,t,r,n,b,cp,c,v,d));
        }
    }
    myfile.close();
}

void Sistema::buscar(int i){

    int flag = 1;
    char opcao;
    std::string sopcao;

    if(i == 0){

        while(flag){
            std::cout << "1.Cidade\n2.Bairro\n3.Titulo\n4.Valor\n0.Voltar" << std::endl;

            std::getline(std::cin, sopcao);
            opcao = sopcao[0];

            switch(opcao){
                case 49:
                    system("clear");
                    buscarCidade(0);
                    break;
                case 50:
                    system("clear");
                    buscarBairro(0);
                    break;
                case 51:
                    system("clear");
                    buscarTitulo(0);
                    break;
                case 52:
                    system("clear");
                    buscarValor(0);
                    break;
                case 48:
                    flag = 0;
                    break;
                default:
                    system("clear");
                    std::cout << "opcao invalida" << std::endl;
                    flag = 1;
                    break;
            }
        }
    }
    if(i == 1){
        while(flag){
            std::cout << "1.Cidade\n2.Bairro\n3.Titulo\n4.Valor\n0.Voltar" << std::endl;

            std::getline(std::cin, sopcao);
            opcao = sopcao[0];

            switch(opcao){
                case 49:
                    system("clear");
                    buscarCidade(1);
                    remover();
                    break;
                case 50:
                    system("clear");
                    buscarBairro(1);
                    remover();
                    break;
                case 51:
                    system("clear");
                    buscarTitulo(1);
                    remover();
                    break;
                case 52:
                    system("clear");
                    buscarValor(1);
                    remover();
                    break;
                case 48:
                    flag = 0;
                    break;
                default:
                    system("clear");
                    std::cout << "opcao invalida" << std::endl;
                    flag = 1;
                    break;
            }
        }

    }
    if(i == 2){
        while(flag){
            std::cout << "1.Cidade\n2.Bairro\n3.Titulo\n4.Valor\n0.Voltar" << std::endl;

            std::getline(std::cin, sopcao);
            opcao = sopcao[0];

            switch(opcao){
                case 49:
                    system("clear");
                    buscarCidade(1);
                    editar();
                    break;
                case 50:
                    system("clear");
                    buscarBairro(1);
                    editar();
                    break;
                case 51:
                    system("clear");
                    buscarTitulo(1);
                    editar();
                    break;
                case 52:
                    system("clear");
                    buscarValor(1);
                    editar();
                    break;
                case 48:
                    flag = 0;
                    break;
                default:
                    system("clear");
                    std::cout << "opcao invalida" << std::endl;
                    flag = 1;
                    break;
            }
        }

    }
}

void Sistema::remover(){
    int flag = 1;
    char opcao,opcao2;
    std::string sopcao,sopcao2;
    int vetor;

    while(flag){
        std::cout << "1.Casa\n2.Apartamento\n3.Terreno\n0.Voltar" << std::endl;
        getline(std::cin,sopcao);
        opcao = sopcao[0];

        switch(opcao){
            case 49:
                system("clear");
                std::cout << "Insira a posicao: " << std::endl;
                std::cin >> vetor;
                std::cin.ignore();
                printaCasa(vetor);
                std::cout<< "Pressione 1 para confirmar, qualquer outra tecla para cancelar" << std::endl;
                getline(std::cin,sopcao2);
                opcao2 = sopcao2[0];
                if(opcao2 == 49){
                    casa.erase(casa.begin() + vetor);
                }
                flag = 0;
                break;
            case 50:
                system("clear");
                std::cout << "Insira a posicao: " << std::endl;
                std::cin >> vetor;
                std::cin.ignore();
                std::cout<< "Pressione 1 para confirmar, qualquer outra tecla para cancelar" << std::endl;
                getline(std::cin,sopcao2);
                opcao2 = sopcao2[0];
                if(opcao2 == 49){
                    apt.erase(apt.begin() + vetor);
                }
                flag = 0;
                break;
            case 51:
                system("clear");
                std::cout << "Insira a posicao: " << std::endl;
                std::cin >> vetor;
                std::cin.ignore();
                std::cout<< "Pressione 1 para confirmar, qualquer outra tecla para cancelar" << std::endl;
                getline(std::cin,sopcao2);
                opcao2 = sopcao2[0];
                if(opcao2 == 49){
                    ter.erase(ter.begin() + vetor);
                }
                flag = 0;
                break;
            case 48:
                system("clear");
                flag = 0;
                break;
            default:
                system("clear");
                std::cout << "opcao invalida" << std::endl;
                flag = 1;
                break;
        }
    }
}

void Sistema::editar(){
    int flag = 1;
    char opcao;
    std::string sopcao;
    int vetor;

    while(flag){
        std::cout << "1.Casa\n2.Apartamento\n3.Terreno\n0.Voltar" << std::endl;
        getline(std::cin,sopcao);
        opcao = sopcao[0];

        switch(opcao){
            case 49:
                system("clear");
                std::cout << "Insira a posicao: " << std::endl;
                std::cin >> vetor;
                std::cin.ignore();
                editaCasa(vetor);
                flag = 0;
                break;
            case 50:
                system("clear");
                std::cout << "Insira a posicao: " << std::endl;
                std::cin >> vetor;
                std::cin.ignore();
                editaApartamento(vetor);
                flag = 0;
                break;
            case 51:
                system("clear");
                std::cout << "Insira a posicao: " << std::endl;
                std::cin >> vetor;
                std::cin.ignore();
                editaTerreno(vetor);
                flag = 0;
                break;
            case 48:
                system("clear");
                flag = 0;
                break;
            default:
                system("clear");
                std::cout << "opcao invalida" << std::endl;
                flag = 1;
                break;
        }
    }
}

void Sistema::editaCasa(int o){
    printaCasa(o);
    int flag = 1;
    char opcao;
    std::string sopcao;
    std::string t;
    int p;
    int q;
    double art;
    double arc;
    std::string r;
    std::string n;
    std::string b;
    std::string cp;
    std::string c;
    double v;
    bool d;

    while(flag){
        std::cout << "1.Titulo\n2.Pavimentos\n3.Quartos\n4.Area do terreno\n5.Area construida\n6.Rua\n7.Numero\n8.Bairro\n9.CEP\na.Cidade\nb.Valor\nc.Disponibilidade\n0.voltar" << std::endl;
        getline(std::cin,sopcao);
        opcao = sopcao[0];

        switch(opcao){
            case 49:
                std::cout << "Titulo:" << std::endl;
                std::getline(std::cin,t);
                casa[o].setTitulo(t);
                break;
            case 50:
                std::cout << "Pavimentos:" << std::endl;
                std::cin >> p;
                std::cin.ignore();
                casa[o].setPavimentos(p);
                break;
            case 51:
                std::cout << "Quartos:" << std::endl;
                std::cin >> q;
                std::cin.ignore();
                casa[o].setQuartos(q);
                break;
            case 52:
                std::cout << "Area do terreno:" << std::endl;
                std::cin >> art;
                std::cin.ignore();
                casa[o].setAreaTerreno(art);
                break;
            case 53:
                std::cout << "Area construida:" << std::endl;
                std::cin >> arc;
                std::cin.ignore();
                casa[o].setAreaConstruida(arc);
                break;
            case 54:
                std::cout << "Rua:" << std::endl;
                std::getline(std::cin,r);
                casa[o].setRua(r);
                break;
            case 55:
                std::cout << "Numero:" << std::endl;
                std::getline(std::cin,n);
                casa[o].setNumero(n);
                break;
            case 56:
                std::cout << "Bairro:" << std::endl;
                std::getline(std::cin,b);
                casa[o].setBairro(b);
                break;
            case 57:
                std::cout << "CEP:" << std::endl;
                std::getline(std::cin,cp);
                casa[o].setCEP(cp);
                break;
            case 97:
                std::cout << "Cidade:" << std::endl;
                std::getline(std::cin,c);
                casa[o].setCidade(c);
                break;
            case 98:
                std::cout << "Valor:" << std::endl;
                std::cin >> v;
                std::cin.ignore();
                casa[o].setValor(v);
                break;
            case 99:
                std::cout << "Disponilibilidade(Alugar = 0/Vender = 1):" << std::endl;
                std::cin >> d;
                casa[o].setDisponibilidade(d);
                break;
            case 48:
                system("clear");
                flag = 0;
                break;
            default:
                system("clear");
                std::cout << "opcao invalida" << std::endl;
                flag = 1;
                break;
        }
    }

}

void Sistema::editaApartamento(int o){
    printaApartamento(o);
    int flag = 1;
    char opcao;
    std::string sopcao;
    std::string t;
    int a;
    int q;
    double art;
    std::string p;
    double con;
    int va;
    std::string r;
    std::string n;
    std::string b;
    std::string cp;
    std::string c;
    double v;
    bool d;

    while(flag){
        std::cout << "1.Titulo\n2.Andar\n3.Quartos\n4.Area\n5.Posicao\n6.Condominio\n7.Vagas de garagem\n8.Rua\n9.Numero\na.Bairro\nb.CEP\nc.Cidade\nd.Valor\ne.Disponibilidade\n0.voltar" << std::endl;
        getline(std::cin,sopcao);
        opcao = sopcao[0];

        switch(opcao){
            case 49:
                std::cout << "Titulo:" << std::endl;
                std::getline(std::cin,t);
                apt[o].setTitulo(t);
                break;
            case 50:
                std::cout << "Andar:" << std::endl;
                std::cin >> a;
                std::cin.ignore();
                apt[o].setAndar(a);
                break;
            case 51:
                std::cout << "Quartos:" << std::endl;
                std::cin >> q;
                std::cin.ignore();
                apt[o].setQuartos(q);
                break;
            case 52:
                std::cout << "Area:" << std::endl;
                std::cin >> art;
                std::cin.ignore();
                apt[o].setAreaTerreno(art);
                break;
            case 53:
                std::cout << "Posicao:" << std::endl;
                std::cin >> p;
                std::cin.ignore();
                apt[o].setPosicao(p);
                break;
            case 54:
                std::cout << "Condominio:" << std::endl;
                std::cin >> con;
                std::cin.ignore();
                apt[o].setCondominio(con);
                break;
            case 55:
                std::cout << "Vagas de garagem:" << std::endl;
                std::cin >> va;
                std::cin.ignore();
                apt[o].setVagas(va);
                break;
            case 56:
                std::cout << "Rua:" << std::endl;
                std::getline(std::cin,r);
                apt[o].setRua(r);
                break;
            case 57:
                std::cout << "Numero:" << std::endl;
                std::getline(std::cin,n);
                apt[o].setNumero(n);
                break;
            case 97:
                std::cout << "Bairro:" << std::endl;
                std::getline(std::cin,b);
                apt[o].setBairro(b);
                break;
            case 98:
                std::cout << "CEP:" << std::endl;
                std::getline(std::cin,cp);
                apt[o].setCEP(cp);
                break;
            case 99:
                std::cout << "Cidade:" << std::endl;
                std::getline(std::cin,c);
                apt[o].setCidade(c);
                break;
            case 100:
                std::cout << "Valor:" << std::endl;
                std::cin >> v;
                std::cin.ignore();
                apt[o].setValor(v);
                break;
            case 101:
                std::cout << "Disponilibilidade(Alugar = 0/Vender = 1):" << std::endl;
                std::cin >> d;
                apt[o].setDisponibilidade(d);
                break;
            case 48:
                system("clear");
                flag = 0;
                break;
            default:
                system("clear");
                std::cout << "opcao invalida" << std::endl;
                flag = 1;
                break;
        }
    }
}


void Sistema::editaTerreno(int o){
    printaTerreno(o);
    int flag = 1;
    char opcao;
    std::string sopcao;
    std::string t;
    double art;
    std::string r;
    std::string n;
    std::string b;
    std::string cp;
    std::string c;
    double v;
    bool d;

    while(flag){
        std::cout << "1.Titulo\n2.Area do terreno\n3.Rua\n4.Numero\n5.Bairro\n6.CEP\n7.Cidade\n8.Valor\n9.Disponibilidade\n0.voltar" << std::endl;
        getline(std::cin,sopcao);
        opcao = sopcao[0];

        switch(opcao){
            case 49:
                std::cout << "Titulo:" << std::endl;
                std::getline(std::cin,t);
                ter[o].setTitulo(t);
                break;
            case 50:
                std::cout << "Area do terreno:" << std::endl;
                std::cin >> art;
                std::cin.ignore();
                ter[o].setAreaTerreno(art);
                break;
            case 51:
                std::cout << "Rua:" << std::endl;
                std::getline(std::cin,r);
                ter[o].setRua(r);
                break;
            case 52:
                std::cout << "Numero:" << std::endl;
                std::getline(std::cin,n);
                ter[o].setNumero(n);
                break;
            case 53:
                std::cout << "Bairro:" << std::endl;
                std::getline(std::cin,b);
                ter[o].setBairro(b);
                break;
            case 54:
                std::cout << "CEP:" << std::endl;
                std::getline(std::cin,cp);
                ter[o].setCEP(cp);
                break;
            case 55:
                std::cout << "Cidade:" << std::endl;
                std::getline(std::cin,c);
                ter[o].setCidade(c);
                break;
            case 56:
                std::cout << "Valor:" << std::endl;
                std::cin >> v;
                std::cin.ignore();
                ter[o].setValor(v);
                break;
            case 57:
                std::cout << "Disponilibilidade(Alugar = 0/Vender = 1):" << std::endl;
                std::cin >> d;
                ter[o].setDisponibilidade(d);
                break;
            case 48:
                system("clear");
                flag = 0;
                break;
            default:
                system("clear");
                std::cout << "opcao invalida" << std::endl;
                flag = 1;
                break;
        }
    }

}


void Sistema::buscarCidade(int o){
    std::string busca;
    std::string aux;
    int a = 0;

    std::cout << "insira cidade:" << std::endl;
    std::getline(std::cin,busca);
    busca = letraGrande(busca);
    for(int i = 0;i < casa.size();i++){
        aux = casa[i].getCidade();
        aux = letraGrande(aux);
        if(strncmp(busca.c_str(), aux.c_str(), busca.size()) == 0){
            if(o == 1)
                std::cout << "Posicao no vetor: " << i << std::endl;
            printaCasa(i);
        }
    }
    for(int i = 0;i < apt.size();i++){
        aux = apt[i].getCidade();
        aux = letraGrande(aux);
        if(strncmp(busca.c_str(), aux.c_str(), busca.size()) == 0){
            if(o == 1)
                std::cout << "Posicao no vetor: " << i << std::endl;
            printaApartamento(i);
        }
    }
    for(int i = 0;i < ter.size();i++){
        aux = ter[i].getCidade();
        aux = letraGrande(aux);
        if(strncmp(busca.c_str(), aux.c_str(), busca.size()) == 0){
            if(o == 1)
                std::cout << "Posicao no vetor: " << i << std::endl;
            printaTerreno(i);
        }
    }

}

void Sistema::buscarBairro(int o){
    std::string busca;
    std::string aux;

    std::cout << "insira Bairro:" << std::endl;
    std::getline(std::cin,busca);
    busca = letraGrande(busca);
    for(int i = 0;i < casa.size();i++){
        aux = casa[i].getBairro();
        aux = letraGrande(aux);
        if(strncmp(busca.c_str(), aux.c_str(), busca.size()) == 0){
            if(o == 1)
                std::cout << "Posicao no vetor: " << i << std::endl;
            printaCasa(i);
        }
    }
    for(int i = 0;i < apt.size();i++){
        aux = apt[i].getBairro();
        aux = letraGrande(aux);
        if(strncmp(busca.c_str(), aux.c_str(), busca.size()) == 0){
            if(o == 1)
                std::cout << "Posicao no vetor: " << i << std::endl;
            printaApartamento(i);
        }
    }
    for(int i = 0;i < ter.size();i++){
        aux = ter[i].getBairro();
        aux = letraGrande(aux);
        if(strncmp(busca.c_str(), aux.c_str(), busca.size()) == 0){
            if(o == 1)
                std::cout << "Posicao no vetor: " << i << std::endl;
            printaTerreno(i);
        }
    }
}

void Sistema::buscarTitulo(int o){
    std::string busca;
    std::string aux;

    std::cout << "insira Titulo:" << std::endl;
    std::getline(std::cin,busca);
    busca = letraGrande(busca);
    for(int i = 0;i < casa.size();i++){
        aux = casa[i].getTitulo();
        aux = letraGrande(aux);
        if(strncmp(busca.c_str(), aux.c_str(), busca.size()) == 0){
            if(o == 1)
                std::cout << "Posicao no vetor: " << i << std::endl;
            printaCasa(i);
        }
    }
    for(int i = 0;i < apt.size();i++){
        aux = apt[i].getTitulo();
        aux = letraGrande(aux);
        if(strncmp(busca.c_str(), aux.c_str(), busca.size()) == 0){
            if(o == 1)
                std::cout << "Posicao no vetor: " << i << std::endl;
            printaApartamento(i);
        }
    }
    for(int i = 0;i < ter.size();i++){
        aux = ter[i].getTitulo();
        aux = letraGrande(aux);
        if(strncmp(busca.c_str(), aux.c_str(), busca.size()) == 0){
            if(o == 1)
                std::cout << "Posicao no vetor: " << i << std::endl;
            printaTerreno(i);
        }
    }
}

void Sistema::buscarValor(int o){

    double valor;
    int flag = 1;
    std::string sopcao;
    std::cout << "Insira valor de corte:" << std::endl;
    std::cin >> valor;
    std::cin.ignore();

    while(flag){
        std::cout << "1.Mais que " << valor << "\n" << "2.Menos que " << valor << std::endl;

        std::getline(std::cin, sopcao);
        if(sopcao == "1"){
            system("clear");
            for(int i = 0;i < casa.size();i++){
                if(casa[i].getValor() >= valor){
                    if(o == 1)
                        std::cout << "Posicao no vetor: " << i << std::endl;
                    printaCasa(i);
                }
            }
            for(int i = 0;i < apt.size();i++){
                if(apt[i].getValor() >= valor){
                    if(o == 1)
                        std::cout << "Posicao no vetor: " << i << std::endl;
                    printaApartamento(i);
                }
            }
            for(int i = 0;i < ter.size();i++){
                if(ter[i].getValor() >= valor){
                    if(o == 1)
                        std::cout << "Posicao no vetor: " << i << std::endl;
                    printaTerreno(i);
                }
            }
            flag = 0;
        }else if(sopcao == "2"){
            system("clear");
            for(int i = 0;i < casa.size();i++){
                if(casa[i].getValor() <= valor){
                    if(o == 1)
                        std::cout << "Posicao no vetor: " << i << std::endl;
                    printaCasa(i);
                }
            }
            for(int i = 0;i < apt.size();i++){
                if(apt[i].getValor() <= valor){
                    if(o == 1)
                        std::cout << "Posicao no vetor: " << i << std::endl;
                    printaApartamento(i);
                }
            }
            for(int i = 0;i < ter.size();i++){
                if(ter[i].getValor() <= valor){
                    if(o == 1)
                        std::cout << "Posicao no vetor: " << i << std::endl;
                    printaTerreno(i);
                }
            }
            flag = 0;

        }else{
            system("clear");
            std::cout << "Opcao invalida" << std::endl;
            flag = 1;
        }
    }

}

std::string Sistema::letraGrande(std::string palavra){ //Transforma a std::string pra maiusculo

    int tamanho = palavra.size(), i;
    unsigned found;

    for(i = 0; i < tamanho; i++){
        found = palavra.find(227); //ã
        if(found != std::string::npos){
            palavra.replace(found, 1,"a");
        }
        found = palavra.find(226); //ã
        if(found != std::string::npos){
            palavra.replace(found, 1,"a");
        }
        found = palavra.find(225); //á
        if(found != std::string::npos){
            palavra.replace(found, 1,"a");
        }
        found = palavra.find(224); //à
        if(found != std::string::npos){
            palavra.replace(found, 1,"a");
        }
        found = palavra.find(195); //Ã
        if(found != std::string::npos){
            palavra.replace(found, 1,"a");
        }
        found = palavra.find(194); //Â
        if(found != std::string::npos){
            palavra.replace(found, 1,"a");
        }
        found = palavra.find(193); //Á
        if(found != std::string::npos){
            palavra.replace(found, 1,"a");
        }
        found = palavra.find(192); //À
        if(found != std::string::npos){
            palavra.replace(found, 1,"a");
        }
        found = palavra.find(234); //ê
        if(found != std::string::npos){
            palavra.replace(found, 1,"e");
        }
        found = palavra.find(233); //130 é
        if(found != std::string::npos){
            palavra.replace(found, 1,"e");
        }
        found = palavra.find(232); //è
        if(found != std::string::npos){
            palavra.replace(found, 1,"e");
        }
        found = palavra.find(202);  //Ê
        if(found != std::string::npos){
            palavra.replace(found, 1,"e");
        }
        found = palavra.find(201); //É
        if(found != std::string::npos){
            palavra.replace(found, 1,"e");
        }
        found = palavra.find(200); //È
        if(found != std::string::npos){
            palavra.replace(found, 1,"e");
        }
        found = palavra.find(245); //õ
        if(found != std::string::npos){
            palavra.replace(found, 1,"o");
        }
        found = palavra.find(244); //ô
        if(found != std::string::npos){
            palavra.replace(found, 1,"o");
        }
        found = palavra.find(243); //ó
        if(found != std::string::npos){
            palavra.replace(found, 1,"o");
        }
        found = palavra.find(242); //ò
        if(found != std::string::npos){
            palavra.replace(found, 1,"o");
        }
        found = palavra.find(213); //Õ
        if(found != std::string::npos){
            palavra.replace(found, 1,"o");
        }
        found = palavra.find(212); //Ô
        if(found != std::string::npos){
            palavra.replace(found, 1,"o");
        }
        found = palavra.find(211); //Ó
        if(found != std::string::npos){
            palavra.replace(found, 1,"o");
        }
        found = palavra.find(210); //Ò
        if(found != std::string::npos){
            palavra.replace(found, 1,"o");
        }
        found = palavra.find(238); //î
        if(found != std::string::npos){
            palavra.replace(found, 1,"i");
        }
        found = palavra.find(237); //í
        if(found != std::string::npos){
            palavra.replace(found, 1,"i");
        }
        found = palavra.find(236); //ì
        if(found != std::string::npos){
            palavra.replace(found, 1,"i");
        }
        found = palavra.find(206); //Î
        if(found != std::string::npos){
            palavra.replace(found, 1,"i");
        }
        found = palavra.find(205); //Í
        if(found != std::string::npos){
            palavra.replace(found, 1,"i");
        }
        found = palavra.find(204); //Ì
        if(found != std::string::npos){
            palavra.replace(found, 1,"i");
        }
        found = palavra.find(251); //û
        if(found != std::string::npos){
            palavra.replace(found, 1,"u");
        }
        found = palavra.find(250); //ú
        if(found != std::string::npos){
            palavra.replace(found, 1,"u");
        }
        found = palavra.find(249); //ù
        if(found != std::string::npos){
            palavra.replace(found, 1,"u");
        }
        found = palavra.find(219); //Û
        if(found != std::string::npos){
            palavra.replace(found, 1,"u");
        }
        found = palavra.find(218); //Ú
        if(found != std::string::npos){
            palavra.replace(found, 1,"u");
        }
        found = palavra.find(217); //Ù
        if(found != std::string::npos){
            palavra.replace(found, 1,"u");
        }
        found = palavra.find(241);//ñ
        if(found != std::string::npos){
            palavra.replace(found, 1,"n");
        }
        found = palavra.find(209); //Ñ
        if(found != std::string::npos){
            palavra.replace(found, 1,"n");
        }
        found = palavra.find(253);//ý
        if(found != std::string::npos){
            palavra.replace(found, 1,"y");
        }
        found = palavra.find(221); //Ý
        if(found != std::string::npos){
            palavra.replace(found, 1,"y");
        }
        found = palavra.find(231);//ç
        if(found != std::string::npos){
            palavra.replace(found, 1,"c");
        }
        found = palavra.find(199); //Ç
        if(found != std::string::npos){
            palavra.replace(found, 1,"c");
        }
    }

    transform(palavra.begin(), palavra.end(), palavra.begin(), ::toupper);
    //cout << "Titulo: " << palavra << endl;

    return palavra;
}
