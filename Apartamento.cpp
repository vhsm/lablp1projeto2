#include "Apartamento.h"

Apartamento::Apartamento(int a, int q, double art, std::string p, double con, int va, std::string t, std::string r, std::string n, std::string b, std::string cp, std::string c, double v, bool d){
    andar = a;
    quartos = q;
    areaTerreno = art;
    posicao = p;
    condominio = con;
    vagas = va;
    titulo = t;
    rua = r;
    numero = n;
    bairro = b;
    CEP = cp;
    cidade = c;
    valor = v;
    disponibilidade = d;
}

Apartamento::~Apartamento(){
}

int Apartamento::getTipo(){
    return tipo;
}

int Apartamento::getAndar(){
    return andar;
}

int Apartamento::getQuartos(){
    return quartos;
}

double Apartamento::getAreaTerreno(){
    return areaTerreno;
}

std::string Apartamento::getPosicao(){
    return posicao;
}

double Apartamento::getCondominio(){
    return condominio;
}

int Apartamento::getVagas(){
    return vagas;
}

void Apartamento::setAndar(int a){
    andar = a;
}

void Apartamento::setQuartos(int q){
    quartos = q;
}

void Apartamento::setAreaTerreno(double art){
    areaTerreno = art;
}

void Apartamento::setPosicao(std::string p){
    posicao = p;
}

void Apartamento::setCondominio(double con){
    condominio = con;
}

void Apartamento::setVagas(int va){
    vagas = va;
}
