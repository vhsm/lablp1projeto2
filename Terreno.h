#ifndef TERRENO_H_
#define TERRENO_H_
#include <iostream>
#include <string>
#include "Imovel.h"

class Terreno : public Imovel{

    private:
        static const int tipo = 2;
        double areaTerreno;
    public:
        Terreno(double art, std::string t, std::string r, std::string n, std::string b, std::string cp, std::string c, double v, bool d);
        double getAreaTerreno();
        int getTipo();
        void setAreaTerreno(double art);
        virtual ~Terreno();

};
#endif
