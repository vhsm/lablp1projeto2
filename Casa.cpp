#include "Casa.h"

Casa::Casa(int p, int q, double art, double arc,std::string t, std::string r, std::string n, std::string b, std::string cp, std::string c, double v, bool d){
    pavimentos = p;
    quartos = q;
    areaTerreno = art;
    areaConstruida = arc;
    titulo = t;
    rua = r;
    numero = n;
    bairro = b;
    CEP = cp;
    cidade = c;
    valor = v;
    disponibilidade = d;
}

Casa::~Casa(){
}

int Casa::getTipo(){
    return tipo;
}

int Casa::getPavimentos(){
    return pavimentos;
}

int Casa::getQuartos(){
    return quartos;
}

double Casa::getAreaTerreno(){
    return areaTerreno;
}

double Casa::getAreaConstruida(){
    return areaConstruida;
}

void Casa::setPavimentos(int p){
    pavimentos = p;
}

void Casa::setQuartos(int q){
    quartos = q;
}

void Casa::setAreaTerreno(double art){
    areaTerreno = art;
}

void Casa::setAreaConstruida(double arc){
    areaConstruida = arc;
}

