#ifndef SISTEMA_H_
#define SISTEMA_H_
#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <algorithm>
#include <cstdio>
#include "Imovel.h"
#include "Casa.h"
#include "Apartamento.h"
#include "Terreno.h"

class Sistema{

    private:
        std::vector<Casa> casa;
        std::vector<Apartamento> apt;
        std::vector<Terreno> ter;
    public:
        void cadastraImovel();
        void criaCasa();
        void criaApartamento();
        void criaTerreno();
        void salvarListaImoveis();
        void recuperarListaImoveis();
        void listar();
        void printaCasa(int i);
        void printaApartamento(int i);
        void printaTerreno(int i);
        void alugaVende(int o);
        void listarCasa(int o);
        void listarApartamento(int o);
        void listarTerreno(int o);
        void buscar(int i);
        void buscarCidade(int o);
        void buscarBairro(int o);
        void buscarTitulo(int o);
        void buscarValor(int o);
        void buscarCasa(std::string busca);
        void buscarApartamento(std::string busca);
        void buscarTerreno(std::string busca);
        void remover();
        void removeCasa(int i);
        void removeApartamento(int i);
        void removeTerreno(int i);
        void editar();
        void editaCasa(int o);
        void editaApartamento(int o);
        void editaTerreno(int o);
        std::string letraGrande(std::string palavra);

};
#endif
