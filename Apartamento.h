#ifndef APARTAMENTO_H_
#define APARTAMENTO_H_
#include <iostream>
#include <string>
#include "Imovel.h"

class Apartamento : public Imovel{

    private:
        static const int tipo = 1;
        int andar;
        int quartos;
        double areaTerreno;
        std::string posicao;
        double condominio;
        int vagas;
    public:
        Apartamento(int a, int q, double art, std::string p, double con, int va, std::string t, std::string r, std::string n, std::string b, std::string cp, std::string c, double v, bool d);
        int getTipo();
        int getAndar();
        int getQuartos();
        double getAreaTerreno();
        std::string getPosicao();
        double getCondominio();
        int getVagas();
        void setAndar(int a);
        void setQuartos(int q);
        void setAreaTerreno(double art);
        void setPosicao(std::string p);
        void setCondominio(double con);
        void setVagas(int va);
        virtual ~Apartamento();

};
#endif
