#include "Terreno.h"

Terreno::Terreno(double art, std::string t, std::string r, std::string n, std::string b, std::string cp, std::string c, double v, bool d){
    areaTerreno = art;
    titulo = t;
    rua = r;
    numero = n;
    bairro = b;
    CEP = cp;
    cidade = c;
    valor = v;
    disponibilidade = d;
}

Terreno::~Terreno(){
}

int Terreno::getTipo(){
    return tipo;
}

double Terreno::getAreaTerreno(){
    return areaTerreno;
}

void Terreno::setAreaTerreno(double art){
    areaTerreno = art;
}
