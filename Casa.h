#ifndef CASA_H_
#define CASA_H_
#include <iostream>
#include <string>
#include "Imovel.h"

class Casa : public Imovel{

    private:
        static const int tipo = 0;
        int pavimentos;
        int quartos;
        double areaTerreno;
        double areaConstruida;
    public:
        Casa(int p, int q, double art, double arc,std::string t, std::string r, std::string n, std::string b, std::string cp, std::string c, double v, bool d);
        int getTipo();
        int getPavimentos();
        int getQuartos();
        double getAreaTerreno();
        double getAreaConstruida();
        void setPavimentos(int p);
        void setQuartos(int q);
        void setAreaTerreno(double art);
        void setAreaConstruida(double arc);
        virtual ~Casa();

};
#endif
